const express = require("express");
const app = express();
const channelRouter = require("./routes/channels/index");
const cors = require("cors");
const db = require("./config/database");

db.authenticate()
  .then(() => console.log(`Database connected...`))
  .catch((err) => console.log("Error : " + err));

app.use(cors());
app.use(express.json());
app.use("/channels", channelRouter);

app.listen(8686, () => console.log(`Server running on http://localhost:8686`));
