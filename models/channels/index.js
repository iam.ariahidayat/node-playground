'use strict';
const db = require("../../config/database");
const DataTypes = require('sequelize/lib/data-types')

const Channel = db.define("channels", {
  name: {
    type: DataTypes.STRING,
  },
  userId: {
    type: DataTypes.INTEGER
  }
});

module.exports = Channel;
