const Channel = require("../../models/channels/index");

const getChannels = async (req, res) => {
  try {
    const channels = await Channel.findAll();
    res.status(200).send(channels);
  } catch (error) {
    console.log(error);
  }
};

const createChannel = async (req, res) => {
  try {
    await Channel.create(req.body);
    res.json({
      message: "Channel created.",
    });
  } catch (error) {
    console.log(error);
  }
};

const getChannel = async (req, res) => {
  try {
    const channel = await Channel.findOne({
      where: {
        id: req.params.id,
      },
    });
    res.status(200).json(channel);
  } catch (error) {
    console.log(error);
  }
};

const updateChannel = async (req, res) => {
  try {
    const channel = await Channel.update(req.body, {
      where: {
        id: req.params.id,
      },
    });
    res.status(200).json({
      message: "Channel updated.",
    });
  } catch (error) {
    console.log(error);
  }
};

const deleteChannel = async (req, res) => {
  try {
    const channel = await Channel.destroy({
      where: {
        id: req.params.id,
      },
    });
    res.status(200).json({
      message: "Channel deleted.",
    });
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  getChannels,
  createChannel,
  getChannel,
  updateChannel,
  deleteChannel,
};
