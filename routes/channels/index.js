const express = require("express");
const router = express.Router();
const {
  getChannels,
  createChannel,
  updateChannel,
  getChannel,
  deleteChannel,
} = require("../../controllers/channels/index");

router.get("/", getChannels);
router.post("/", createChannel);
router.get("/:id", getChannel);
router.put("/:id", updateChannel);
router.delete("/:id", deleteChannel);

module.exports = router;
